init_light_folder:
	cd tileserver-gl && node publish.js || echo "osef si ça foire"
	sed -i "1s/.*/FROM node:10.15.1-alpine/" tileserver-gl/light/Dockerfile


build_base_image: init_light_folder
	cd tileserver-gl/light && docker build --tag floawfloaw/light-world-tileserver:latest .

build_image_8: build_base_image
	docker build --build-arg ZOOM=8 --tag floawfloaw/light-world-tileserver:latest--zoom-8 .

build_image_11: build_base_image
	docker build --build-arg ZOOM=11 --tag floawfloaw/light-world-tileserver:latest--zoom-11 .

run:
	docker run --rm -it -p 8080:80 floawfloaw/light-world-tileserver:latest--zoom-8