Ansible -- setup server for computing tiles
======================

Make sure you can connect to the remote server of your choice :smile:

* Install `ansible`

* Install `ansible-galaxy` dependencies:
    - `ansible-galaxy install -r requirements.yml`

* Setup env variables:
    - Change the `IP` of the remote host server in `inventory.yml`

* Run:

```bash
ansible-playbook -i inventory.yml playbook.yml 
```

:confetti_ball: