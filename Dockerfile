# Use the Makefile to make sure the base image is available
FROM floawfloaw/light-world-tileserver:latest

ARG ZOOM
COPY ./data/${ZOOM}.mbtiles /data/${ZOOM}.mbtiles
