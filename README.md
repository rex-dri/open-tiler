Open-tiler
==========

A simple way to generate vector map tiles of the entire world.

## Setup

In the `ansible` directory you will find the *ansible* scripts to setup a remote server with all the dependencies needed to compute the tiles.

## To compute the tiles

(after you have run the ansible script)

```bash
cd /usr/src/openmaptiles
mkdir data
cd data
# Download the OSM data for the whole world (~50GB as of 2019-12-05)
wget -O planet.osm.pbf https://planet.osm.org/pbf/planet-latest.osm.pbf
```

Then edit the `.env` file:
```bash
cd /usr/src/openmaptiles
nano .env
```

And set:
- `QUICKSTART_MAX_ZOOM=11`
- `MAX_ZOOM=11` (not sure if this one is 100% useful)

Then, run `tmux` and:
```bash
cd /usr/src/openmaptiles
./quickstart.sh planet
```

The process should take a bit more than 24 hours on reasonably sized server (you need at least 600-700GB of disk space).


## Generating lower zoom mbtiles

To reduce the zoom level of the mbtiles (and reduce therefore the size), you need to use:

```bash
sqlite3 8.mbtiles
```

Then use the following SQL commands (taken from [here](https://gist.github.com/zhm/3830471)):

```SQL
DELETE FROM images WHERE 
  tile_id IN (SELECT tile_id FROM map WHERE zoom_level > 8) AND
  tile_id NOT IN (SELECT tile_id FROM map WHERE zoom_level <= 8);

DELETE FROM map WHERE zoom_level > 8;

UPDATE metadata SET value = '8' WHERE name = 'maxzoom';
```

Make sure to finish by the following command to rebuild the `mbtiles` file:

```SQL
VACUUM;
```

## Building the images

The image provided by https://github.com/klokantech/tileserver-gl was not up-to-date and not small enough (and I am lazy to do a PR for now). So this repo contains a submodule pointing a specific commit of this repo.

There is a Makefile to automate the creation process of the images.

docker run --rm -it -p 8080:80 floawfloaw/light-world-tileserver:8-latest


## Other credit

Computation of the map tiles was acheived with the support of *#béber* (thank you for having contributed your free credits dude).

